DIM=2
SIZE=1024
./diffusion_cuda.exe --nd ${DIM} --size ${SIZE}           
./diffusion_cuda_shared3.exe --nd ${DIM} --size ${SIZE} 
./diffusion_cuda_opt1.exe --nd ${DIM} --size ${SIZE}        
./diffusion_cuda_shared4.exe --nd ${DIM} --size ${SIZE} 
./diffusion_cuda_opt2.exe--nd ${DIM} --size ${SIZE}  
./diffusion_cuda_shared5.exe --nd ${DIM} --size ${SIZE}   
./diffusion_cuda_shared6.exe --nd ${DIM} --size ${SIZE} 
./diffusion_cuda_restrict.exe --nd ${DIM} --size ${SIZE}    
./diffusion_cuda_shfl1.exe --nd ${DIM} --size ${SIZE} 
./diffusion_cuda_roc.exe --nd ${DIM} --size ${SIZE}         
./diffusion_cuda_shfl2.exe --nd ${DIM} --size ${SIZE} 
./diffusion_cuda_shared1.exe --nd ${DIM} --size ${SIZE}    
./diffusion_cuda_zblock.exe --nd ${DIM} --size ${SIZE} 
./diffusion_cuda_shared2.exe --nd ${DIM} --size ${SIZE} 
./diffusion_cuda_regcache1.exe --nd ${DIM} --size ${SIZE} 