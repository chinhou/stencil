#ifndef DIFFUSION_DIFFUSION_CUDA_SHARED_H_
#define DIFFUSION_DIFFUSION_CUDA_SHARED_H_

#include "diffusion_cuda.h"

namespace diffusion {

	class DiffusionCUDARegcache1 : public DiffusionCUDA {
	public:
		DiffusionCUDARegcache1(int nd, const int *dims) :
			DiffusionCUDA(nd, dims) {
			// 3D is not yet implemented 
			//assert(nd == 2);
		}
		virtual std::string GetName() const {
			return std::string("cuda_regcache1");
		}
		virtual std::string GetDescription() const {
			return std::string("cuda_regcache1 + implemented by chen peng");
		}
		virtual void Setup();
		virtual void RunKernel(int count);
	};

	__device__ __host__ __forceinline__ unsigned int UpDivide(unsigned int x, unsigned int y) { assert(y != 0); return (x + y - 1) / y; }
	__device__ __host__ __forceinline__ unsigned int UpRound(unsigned int x, unsigned int y) { return UpDivide(x, y)*y; }


} // namespace diffusion

#endif // DIFFUSION_DIFFUSION_CUDA_OPT_H_
