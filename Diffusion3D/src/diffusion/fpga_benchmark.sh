#!/bin/bash

runs=1
iter=500
input_size=15000
folder=stratixv

for i in `ls $folder | grep RAD | grep -v aocx | grep -v aoco | grep -v "\.cl" | sort -V`
do
	echo "$i" | xargs printf "%-55s"
	RAD=`echo $i | cut -d "_" -f 3 | cut -c 4-`
	TIME=`echo $i | cut -d "_" -f 4 | cut -c 5-`
	ASIZE=`echo $i | cut -d "_" -f 5 | cut -c 6-`
	BSIZE=`echo $i | cut -d "_" -f 6 | cut -c 6-`
	freq=`cat $folder/$i/acl_quartus_report.txt | grep Kernel | cut -d " " -f 3`
	compute_bsize=$(($BSIZE - (2 * $RAD * $TIME)))
	last_col=$(($input_size + $compute_bsize - $input_size	% $compute_bsize))
	#last_col=640
	timesum=0
	flopssum=0
	bytessum=0
	make clean >/dev/null 2>&1; make altera-host BOARD=de5net_a7 RAD=$RAD TIME=$TIME ASIZE=$ASIZE BSIZE=$BSIZE >/dev/null 2>&1 
	rm diffusion_opencl.aocx 2>&1
	ln -s "$folder"/"$i".aocx diffusion_opencl.aocx
	for (( k=1; k<=$runs; k++ ))
	do
		out=`CL_DEVICE_TYPE=CL_DEVICE_TYPE_ACCELERATOR ./diffusion_altera.exe --size $last_col --count $iter --nd 2 2>&1`
		time=`echo "$out" | grep "Kernel-only" -A 5 | grep time | cut -d " " -f 4`
		flops=`echo "$out" | grep "Kernel-only" -A 5 | grep FLOPS | cut -d " " -f 10`
		bytes=`echo "$out" | grep "Kernel-only" -A 5 | grep Throughput | cut -d " " -f 5`
		timesum=`echo $timesum+$time | bc -l`
		flopssum=`echo $flopssum+$flops | bc -l`
		bytessum=`echo $bytessum+$bytes | bc -l`
	done
	timeaverage=`echo $timesum/$runs | bc -l`
	flopsaverage=`echo $flopssum/$runs | bc -l`
	bytesaverage=`echo $bytessum/$runs | bc -l`
	echo $freq | xargs printf "%-10.3f"
	echo $last_col | xargs printf "%-10d"
	echo $timeaverage | xargs printf "%-10.3f"
	echo $flopsaverage | xargs printf "%-10.3f"
	echo $bytesaverage | xargs printf "%-10.3f"
	echo
done
