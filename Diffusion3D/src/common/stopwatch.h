// Copyright 2011, Tokyo Institute of Technology.
// All rights reserved.
//
// This file is distributed under the license described in
// LICENSE.txt.
//
// Author: Naoya Maruyama (naoya@matsulab.is.titech.ac.jp)

#ifndef BENCHMARKS_COMMON_STOPWATCH_H_
#define BENCHMARKS_COMMON_STOPWATCH_H_

#if defined(unix) || defined(__unix__) || defined(__unix) || \
  defined(__APPLE__)
#include <sys/time.h>
#include <time.h>

typedef struct {
  struct timeval tv;
} Stopwatch;

#else
//#error "Unknown environment"
#pragma message("windows")
#include <Winsock2.h>
typedef struct {
	struct timeval tv;
} Stopwatch;

#include    <windows.h>
#include    <time.h>

struct timezone {
	int tz_minuteswest;
	int tz_dsttime;
};

#define EPOCHFILETIME (116444736000000000i64)

inline int gettimeofday(struct timeval *tv, struct timezone *tz)
{
	FILETIME        tagFileTime;
	LARGE_INTEGER   largeInt;
	__int64         val64;
	static int      tzflag;

	if (tv)
	{
		GetSystemTimeAsFileTime(&tagFileTime);

		largeInt.LowPart = tagFileTime.dwLowDateTime;
		largeInt.HighPart = tagFileTime.dwHighDateTime;
		val64 = largeInt.QuadPart;
		val64 = val64 - EPOCHFILETIME;
		val64 = val64 / 10;
		tv->tv_sec = (long)(val64 / 1000000);
		tv->tv_usec = (long)(val64 % 1000000);
	}

	if (tz)
	{
		if (!tzflag)
		{
			_tzset();
			tzflag++;
		}

		//Visual C++ 6.0でＯＫだった・・
		//tz->tz_minuteswest = _timezone / 60;
		//tz->tz_dsttime = _daylight;

		long _Timezone = 0;
		_get_timezone(&_Timezone);
		tz->tz_minuteswest = _Timezone / 60;

		int _Daylight = 0;
		_get_daylight(&_Daylight);
		tz->tz_dsttime = _Daylight;
	}

	return 0;
}

#endif

static inline void StopwatchQuery(Stopwatch *w) {
  gettimeofday(&(w->tv), NULL);
  return;
}

static inline float StopwatchDiff(const Stopwatch *begin,
                                  const Stopwatch *end) {
  return (end->tv.tv_sec - begin->tv.tv_sec)
      + (end->tv.tv_usec - begin->tv.tv_usec) * 1.0e-06;
}

static inline void StopwatchStart(Stopwatch *w) {
  StopwatchQuery(w);
  return;
}
    
static inline float StopwatchStop(Stopwatch *w) {
  Stopwatch now;
  StopwatchQuery(&now);
  return StopwatchDiff(w, &now);
}

#endif /* BENCHMARKS_COMMON_STOPWATCH_H_ */
