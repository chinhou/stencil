#ifndef DIFFUSION_DIFFUSION_CUDA_SHARED_H_
#define DIFFUSION_DIFFUSION_CUDA_SHARED_H_

#include "diffusion3d_cuda.h"

namespace diffusion3d {

	class Diffusion3DCUDARegcache1 : public Diffusion3DCUDA {
	public:
		Diffusion3DCUDARegcache1(int nx, int ny, int nz) :
			Diffusion3DCUDA(nx, ny, nz) {
			// block_x_ = 32;
			// block_y_ = 4;
		}
		virtual std::string GetName() const {
			return std::string("cuda_register cache1");
		}
		virtual void InitializeBenchmark();
		virtual void RunKernel(int count);
	};

	__device__ __host__ __forceinline__ unsigned int UpDivide(unsigned int x, unsigned int y) { assert(y != 0); return (x + y - 1) / y; }
	__device__ __host__ __forceinline__ unsigned int UpRound(unsigned int x, unsigned int y) { return UpDivide(x, y)*y; }


} // namespace diffusion

#endif // DIFFUSION_DIFFUSION_CUDA_OPT_H_
