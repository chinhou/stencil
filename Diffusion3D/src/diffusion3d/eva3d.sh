#!/bin/bash
echo "-------------begin evaluation------------------------------------------------------"
SIZE=512
./diffusion_cuda.exe --size ${SIZE}      
./diffusion_cuda_shared1.exe --size ${SIZE}    
./diffusion_cuda_shared4.exe --size ${SIZE}    
./diffusion_cuda_shfl1.exe --size ${SIZE}  
./diffusion_cuda_opt1.exe --size ${SIZE}    
./diffusion_cuda_restrict.exe --size ${SIZE}     
./diffusion_cuda_shared2.exe --size ${SIZE}   
./diffusion_cuda_shared5.exe --size ${SIZE}  
./diffusion_cuda_shfl2.exe --size ${SIZE}  
./diffusion_cuda_opt2.exe --size ${SIZE}  
./diffusion_cuda_roc.exe --size ${SIZE}          
./diffusion_cuda_shared3.exe --size ${SIZE}    
./diffusion_cuda_shared6.exe --size ${SIZE}   
./diffusion_cuda_zblock.exe --size ${SIZE}  
./diffusion_cuda_regcache1.exe --size ${SIZE} 
