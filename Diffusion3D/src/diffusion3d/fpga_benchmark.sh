#!/bin/bash

runs=5
iter=1000
folder=stratixv

for i in {1..5}
do
	echo "$i" | xargs printf "%-4s"
	freq=`ls $folder | grep diffusion3d_v2.3.2_TIME"$i"_ASIZE8_BSIZE256_MTCR1 | grep aocx | cut -d "_" -f 7 | sed 's/.aocx//g'`
	name=diffusion3d_v2.3.2_TIME"$i"_ASIZE8_BSIZE256_MTCR1_"$freq"
	#echo $name
	compute_bsize=$((256 - (2 * $i)))
	#last_col=$((512 + $compute_bsize - 512 % $compute_bsize))
	last_col=640
	timesum=0
	flopssum=0
	bytessum=0
	make clean >/dev/null 2>&1; make altera-host BOARD=de5net_a7 TIME=$i ASIZE=8 MTCR=1 BSIZE=256 >/dev/null 2>&1 
	rm diffusion3d_opencl.aocx
	ln -s "$folder"/"$name".aocx diffusion3d_opencl.aocx
	for (( k=1; k<=$runs; k++ ))
	do
		out=`CL_DEVICE_TYPE=CL_DEVICE_TYPE_ACCELERATOR ./diffusion3d_altera.exe --size $last_col --count $iter 2>&1`
		time=`echo "$out" | grep "Kernel-only" -A 5 | grep time | cut -d " " -f 4`
		flops=`echo "$out" | grep "Kernel-only" -A 5 | grep FLOPS | cut -d " " -f 10`
		bytes=`echo "$out" | grep "Kernel-only" -A 5 | grep Throughput | cut -d " " -f 5`
		timesum=`echo $timesum+$time | bc -l`
		flopssum=`echo $flopssum+$flops | bc -l`
		bytessum=`echo $bytessum+$bytes | bc -l`
	done
	timeaverage=`echo $timesum/$runs | bc -l`
	flopsaverage=`echo $flopssum/$runs | bc -l`
	bytesaverage=`echo $bytessum/$runs | bc -l`
	echo $freq | xargs printf "%-10.3f"
	echo $last_col | xargs printf "%-10d"
	echo $timeaverage | xargs printf "%-10.3f"
	echo $flopsaverage | xargs printf "%-10.3f"
	echo $bytesaverage | xargs printf "%-10.3f"
	echo
done
