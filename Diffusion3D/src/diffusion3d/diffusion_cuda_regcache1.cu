#define USE_LDG
#include "diffusion_cuda_regcache.h"
#include "common/cuda_util.h"
#define CUDA_SAFE_CALL(c)                       \
  do {                                          \
    assert(c == cudaSuccess);                   \
  } while (0)

#ifndef NOUSE_MAD
template<typename T> __device__ __forceinline__ T MAD(T a, T b, T c);

template<> __device__ __forceinline__ int MAD<int>(int a, int b, int c) {
	asm("mad.wide.s32 %0, %1, %2, %3;" : "=r"(c) : "r"(a), "r"(b), "r"(c));
	return c;
}
typedef unsigned int uint;
template<> __device__ __forceinline__  uint MAD<uint>(uint a, uint b, uint c) {
	asm("mad.wide.u32 %0, %1, %2, %3;" : "=r"(c) : "r"(a), "r"(b), "r"(c));
	return c;
}

template<> __device__ __forceinline__  float MAD<float>(float a, float b, float c) {
	float d;
	asm volatile("mad.rz.ftz.f32 %0,%1,%2,%3;" : "=f"(d) : "f"(a), "f"(b), "f"(c));
	return d;
}
#if (CUDART_VERSION >= 9000)
#pragma message("CUDART_VERSION >= 9000")
#define __my_shfl_up(var, delta) __shfl_up_sync(0xFFFFFFFF, var, delta)
#define __my_shfl_down(var, delta) __shfl_down_sync(0xFFFFFFFF, var, delta)
#else
#pragma message("CUDART_VERSION < 9000")
#define __my_shfl_up(var, delta) __shfl_up(var, delta)
#define __my_shfl_down(var, delta) __shfl_down(var, delta)
#endif

#endif //NOUSE_MAD

static const int WARP_SIZE = 32;
namespace diffusion3d {
	namespace cuda_regcache1 {

		template<typename T, int BLOCK_SIZE, int PROCESS_DATA_COUNT>
		__global__ void kernel2d_3x3(const REAL * __restrict__ src, REAL* dst,
			int nx, int ny,
			T ce, T cw, T cn, T cs,
			T cc) {
			const int FILTER_WIDTH = 3;
			const int FILTER_HEIGHT = 3;
			const int WARP_COUNT = BLOCK_SIZE >> 5;
			const int laneId = threadIdx.x & 31;
			const int warpId = threadIdx.x >> 5;
			const int WARP_PROCESS_DATA_COUNT = WARP_SIZE - FILTER_WIDTH + 1;
			const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;
			const int DATA_CACHE_SIZE = PROCESS_DATA_COUNT + FILTER_HEIGHT - 1;

			T data[DATA_CACHE_SIZE];

#define width nx
#define height ny
			const int process_count = BLOCK_PROCESS_DATA_COUNT*blockIdx.x + WARP_PROCESS_DATA_COUNT*warpId;
			if (process_count >= width)
				return;
			int tidx = process_count + laneId - FILTER_WIDTH / 2;
			int tidy = PROCESS_DATA_COUNT*blockIdx.y - FILTER_HEIGHT / 2;


			int index = nx*tidy + tidx;
			{
				#pragma unroll
				for (int s = 0; s < DATA_CACHE_SIZE; s++) {
					int _tidy = tidy + s;
					if (tidx < 0) {
						if (_tidy < 0) {
							data[s] = src[index - tidx - _tidy*nx];
						}
						else if (_tidy < height) {
							data[s] = src[index - tidx];
						}else {
							assert(index - tidx - (_tidy - ny + 1)*nx >= 0);
							assert(index - tidx - (_tidy - ny + 1)*nx < nx*ny);
							data[s] = src[index - tidx - (_tidy - ny + 1)*nx];
						}
					}
					else if (tidx < width) {
						if (_tidy < 0) {
							data[s] = src[index - _tidy*nx];
						}
						else if (_tidy < height) {
							data[s] = src[index];
						}
						else {
							data[s] = src[index - tidx - (_tidy - ny + 1)*nx];
						}
					}
					else {
						if (_tidy < 0) {
							data[s] = src[index - (tidx - nx + 1) - _tidy*nx];
						}
						else if (_tidy < height) {
							data[s] = src[index - (tidx - nx + 1)];
						}
						else {
							data[s] = src[index - (tidx - nx + 1) - (_tidy - ny + 1)*nx];
						}
					}
					index += nx;
				}
			}
			T* p = &data[0];
#pragma unroll
			for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
				T sum;

				{
					//m = 0
					sum = data[i + 1] * cw;
				}
				{
					//m = 1
					sum = __my_shfl_up(sum, 1);
					sum = MAD(data[i + 0], cn, sum);
					sum = MAD(data[i + 1], cc, sum);
					sum = MAD(data[i + 2], cs, sum);
				}
				{
					//m = 2
					sum = __my_shfl_up(sum, 1);
					sum = MAD(data[i + 1], ce, sum);
				}
				data[i] = sum;
			}

			index = nx*(tidy + FILTER_HEIGHT / 2) + tidx - (FILTER_WIDTH - 1) / 2;
#pragma unroll
			for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
				if (laneId >= FILTER_WIDTH - 1 && tidx - (FILTER_WIDTH - 1) / 2 < width && tidy + FILTER_HEIGHT / 2 + i < height) {
					dst[index] = data[i];
				}
				index += nx;
			}
#undef width 
#undef height 
		}


		template<typename T, int FILTER_SIZE, int PROCESS_DATA_COUNT_Y, int PROCESS_DATA_COUNT_Z, int WARP_COUNT>
		__global__ void kernel3d_3x3x3(const REAL * __restrict__ src, REAL* dst,
			int nx, int ny, int nz, int nxy,
			REAL ce, REAL cw, REAL cn, REAL cs,
			REAL ct, REAL cb, REAL cc) 
		{
			const int FILTER_WIDTH = FILTER_SIZE;
			const int FILTER_HEIGHT = FILTER_SIZE;
			const int FILTER_DEPTH = FILTER_SIZE;
			const int laneId = threadIdx.x;
			const int warpId = threadIdx.y;
			const int WARP_PROCESS_DATA_COUNT_X = WARP_SIZE - FILTER_WIDTH + 1;
			const int DATA_CACHE_SIZE = PROCESS_DATA_COUNT_Y + FILTER_HEIGHT - 1;

			__shared__ T sMem[WARP_COUNT][DATA_CACHE_SIZE][WARP_SIZE];

			T data[DATA_CACHE_SIZE];

#define width nx
#define height ny
#define depth  nz
			const int process_count_x = WARP_PROCESS_DATA_COUNT_X*blockIdx.x;
			const int process_count_y = PROCESS_DATA_COUNT_Y*blockIdx.y;
			const int process_count_z = PROCESS_DATA_COUNT_Z*blockIdx.z;

			const int tidx = process_count_x + laneId - FILTER_WIDTH / 2;
			const int tidy = process_count_y - FILTER_HEIGHT / 2;
			const int tidz = process_count_z + warpId - FILTER_DEPTH / 2;

			//if (/*blockIdx.x == 0 && */blockIdx.y == 9 && blockIdx.z == 8) {
			//	//printf(".");
			//}

			int index = nxy*tidz + nx*tidy + tidx;
			{
#pragma unroll
				for (int s = 0; s < DATA_CACHE_SIZE; s++) {
					int _tidy = tidy + s;
					int _idx = index;
					if (tidx < 0)        _idx -= tidx;
					else if (tidx >= nx) _idx -= tidx - nx + 1;

					if (_tidy < 0)        _idx -= _tidy*nx;
					else if (_tidy >= ny) _idx -= (_tidy - ny + 1)*nx;

					if (tidz < 0)        _idx -= tidz*nxy;
					else if (tidz >= nz) _idx -= (tidz - nz + 1)*nxy;

					//if (_idx < 0 || _idx >= nz*nxy)
					{
						//assert(_idx < 0 || _idx >= nz*nxy);
					}
					data[s] = src[_idx];
					sMem[warpId][s][laneId] = data[s];
					index += nx;
				}
			}
			if (warpId < FILTER_DEPTH / 2 || warpId >= PROCESS_DATA_COUNT_Z + FILTER_DEPTH / 2) {
				return;
			}
			__syncthreads();
#pragma unroll
			for (int i = 0; i < PROCESS_DATA_COUNT_Y; i++) {
				T sum;
				{
					//m = 0
					sum = data[i + 1] * cw;
				}
				{
					//m = 1
					sum = __my_shfl_up(sum, 1);
					sum = MAD(data[i + 0], cn, sum);
					sum = MAD(data[i + 1], cc, sum);
					sum = MAD(data[i + 2], cs, sum);
				}
				{
					//m = 2
					sum = __my_shfl_up(sum, 1);
					sum = MAD(data[i + 1], ce, sum);
				}
				{
					int x = laneId - FILTER_WIDTH / 2;
					int y = i + FILTER_HEIGHT / 2;
					if (x >= FILTER_WIDTH / 2 && x < nx - FILTER_WIDTH / 2
						&& y >= FILTER_HEIGHT / 2 && y < ny - FILTER_HEIGHT / 2)
					{
						sum = MAD(sMem[warpId - 1][y][x], ct, sum);
						sum = MAD(sMem[warpId + 1][y][x], cb, sum);
					}
				}
				data[i] = sum;
			}

			if (laneId >= FILTER_WIDTH - 1) 
			{
				int _tidy = tidy + FILTER_HEIGHT / 2;
				int _tidx = tidx - (FILTER_WIDTH - 1) / 2;
				index = tidz*nxy + nx*_tidy + _tidx;
#pragma unroll
				for (int i = 0; i < PROCESS_DATA_COUNT_Y; i++) {
					//if (laneId >= FILTER_WIDTH - 1)
					{
						if (_tidx >= 0 && _tidx < nx && _tidy < ny && tidz >= 0 && tidz < nz) {
							dst[index] = data[i];
							//dst[index] = index;
						}
					}
					_tidy++;
					index += nx;
				}
			}
#undef laneId
#undef warpId
		}
	} // namespace cuda_regcache1

	void Diffusion3DCUDARegcache1::RunKernel(int count) {
		int flag = 0;
		{
			//3D
			typedef REAL DataType;	
			const int FILTER_SIZE = 3;
			const int FILTER_WIDTH = FILTER_SIZE;
			const int FILTER_HEIGHT = FILTER_SIZE;
			const int FILTER_DEPTH = FILTER_SIZE;
			const int PROCESS_DATA_COUNT = 8;
			const int WARP_COUNT = 14 + (FILTER_DEPTH / 2) * 2;

			const int BLOCK_PROCESS_DATA_COUNT_X = WARP_SIZE - FILTER_WIDTH + 1;
			const int BLOCK_PROCESS_DATA_COUNT_Y = PROCESS_DATA_COUNT;
			const int BLOCK_PROCESS_DATA_COUNT_Z = WARP_COUNT - (FILTER_DEPTH / 2) * 2;

			dim3 block_size(WARP_SIZE, WARP_COUNT);
			dim3 grid_size(UpDivide(nx_, BLOCK_PROCESS_DATA_COUNT_X),
				UpDivide(ny_, BLOCK_PROCESS_DATA_COUNT_Y),
				UpDivide(nz_, BLOCK_PROCESS_DATA_COUNT_Z)
				);

			size_t s = sizeof(REAL) * nx_ * ny_ * nz_;
			FORCE_CHECK_CUDA(cudaMemcpy(f1_d_, f1_, s, cudaMemcpyHostToDevice));

#pragma omp parallel num_threads(2) shared(flag)
			{
				if (omp_get_thread_num() == 0)
				{
					power = GetPowerGPU(&flag, 0);
				}
				else
				{
#pragma omp barrier
					CUDA_SAFE_CALL(cudaEventRecord(ev1_));
					for (int i = 0; i < count; ++i) {
						{
							cuda_regcache1::kernel3d_3x3x3<DataType, FILTER_SIZE, BLOCK_PROCESS_DATA_COUNT_Y, BLOCK_PROCESS_DATA_COUNT_Z, WARP_COUNT> << <grid_size, block_size >> >
								(f1_d_, f2_d_, nx_, ny_, nz_, nx_*ny_, ce_, cw_, cn_, cs_, ct_, cb_, cc_);
						}
						REAL *t = f1_d_;
						f1_d_ = f2_d_;
						f2_d_ = t;
					}
					CUDA_SAFE_CALL(cudaEventRecord(ev2_));
					CUDA_SAFE_CALL(cudaDeviceSynchronize());
					flag = 1;
				}
			}
			CUDA_SAFE_CALL(cudaMemcpy(f1_, f1_d_, s, cudaMemcpyDeviceToHost));
		}
		return;
	}

	void Diffusion3DCUDARegcache1::InitializeBenchmark() {
		Diffusion3DCUDA::InitializeBenchmark();
		//FORCE_CHECK_CUDA(cudaFuncSetCacheConfig(cuda_regcache1::kernel3d,
		//	cudaFuncCachePreferShared));
	}
} // namespace diffusion

