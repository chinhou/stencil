#!/bin/bash
clear
echo "-------------begin evaluation------------------------------------------------------"
SIZE=512  
./diffusion3d_cuda_regcache1.exe   --size ${SIZE}  
./diffusion3d_cuda.exe        --size ${SIZE}  
./diffusion3d_cuda_opt0.exe   --size ${SIZE}  
./diffusion3d_cuda_opt1.exe   --size ${SIZE}  
./diffusion3d_cuda_opt2.exe  --size ${SIZE}   
./diffusion3d_cuda_opt3.exe    --size ${SIZE}   
./diffusion3d_cuda_shared.exe   --size ${SIZE}   
./diffusion3d_cuda_shared1.exe     --size ${SIZE}    
./diffusion3d_cuda_shared2.exe  --size ${SIZE}   
./diffusion3d_cuda_shared3.exe  --size ${SIZE} 
./diffusion3d_cuda_shared4.exe   --size ${SIZE}  
./diffusion3d_cuda_shared5.exe   --size ${SIZE}
./diffusion3d_cuda_shared6.exe    --size ${SIZE}   
./diffusion3d_cuda_temporal_blocking.exe   --size ${SIZE}    
./diffusion3d_openmp_temporal_blocking.exe --size ${SIZE}      
./diffusion3d_cuda_zblock.exe --size ${SIZE}            
./diffusion3d_openmp.exe --size ${SIZE}  
./diffusion3d_cuda_xy.exe --size ${SIZE}  

